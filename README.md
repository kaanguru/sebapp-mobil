# SebApp

> Kurenk Kulubu için güvercin takip uygulaması

48 veri alanı
imagezoom ve toasty eklentileri elle yüklenecek


## Usage

``` bash
# Install dependencies
yarn

# Preview on device
tns preview

# Build, watch for changes and run the application
tns run

# Build, watch for changes and debug the application
tns debug <platform>

# Build for production
tns build <platform> --env.production

# update without ns ui

ncu -u -x "/^nativescript-ui.*$/"
```
