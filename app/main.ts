import Vue from "nativescript-vue";
import { ApplicationSettings } from "@nativescript/core";
import VueApollo from "vue-apollo";
import {
    ApolloClient,
    InMemoryCache,
    HttpLink,
    ApolloLink,
} from "apollo-boost";
import { onError } from "apollo-link-error";
import { setContext } from "apollo-link-context";
import { upperFirst, camelCase } from "lodash";
import RadDataForm from "nativescript-ui-dataform/vue";
import Vuelidate from "vuelidate";
import routes from "./routes";
import { BACKEND, SILENCE } from "./helpers/config";

Vue.use(RadDataForm);
Vue.use(Vuelidate);

/////////////// Auto main components
const requireComponent = require.context(
    "./components",
    false,
    /Main[A-Z]\w+\.(vue|js)$/
);
requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);
    const componentName = upperFirst(
        camelCase(
            fileName
                .split("/")
                .pop()
                .replace(/\.\w+$/, "")
        )
    );

    Vue.component(componentName, componentConfig.default || componentConfig);
});

/////////////// APOLLO
Vue.use(VueApollo);
const errorLink = onError(({ graphQLErrors }) => {
    if (graphQLErrors) graphQLErrors.map(({ message }) => console.log(message));
});
const httpLink = new HttpLink({
    uri: BACKEND + "/graphql",
});
const authLink = setContext((_, { headers }) => {
    // get the authentication token from ApplicationSettings if it exists
    var tokenInAppSettings = ApplicationSettings.getString("token");
    // return the headers to the context so HTTP link can read them
    return {
        headers: {
            ...headers,
            authorization: tokenInAppSettings
                ? `Bearer ${tokenInAppSettings}`
                : null,
        },
    };
});
export const apolloClient = new ApolloClient({
    link: ApolloLink.from([errorLink, authLink, httpLink]),
    cache: new InMemoryCache(),
});

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
});

/////////////// MENU
Vue.registerElement(
    "RadSideDrawer",
    () => require("nativescript-ui-sidedrawer").RadSideDrawer
);

/////////////// CONSOLE

if (TNS_ENV !== "production") {
    Vue.config.silent = SILENCE;
    // @ts-ignore
    Vue.config.debug = !SILENCE;
}

///////////////  floating action button
Vue.registerElement(
    "Fab",
    () => require("@nstudio/nativescript-floatingactionbutton").Fab
);

///////////////  ImageZoom
Vue.registerElement(
    "ImageZoom",
    () => require("@triniwiz/nativescript-image-zoom").ImageZoom
);

///////////////
new Vue({
    apolloProvider,
    render: (h) =>
        h("frame", [
            h(
                ApplicationSettings.hasKey("token")
                    ? routes.firstLoggedIn
                    : routes.login
            ),
        ]),
}).$start();
