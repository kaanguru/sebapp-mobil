// AUTH
import login from "@/views/auth/Login.vue";
import register from "@/views/auth/Register.vue";
import firstLoggedIn from "@/views/auth/FirstLoggedIn.vue";
import accountExpired from "@/views/auth/AccountExpired.vue";
import unBlockRequest from "@/views/auth/UnBlockRequest.vue";
import exit from "@/views/auth/Exit.vue";

// PERSONAL
import selfie from "@/views/personal/AddSelfie.vue";
import editUser from "@/views/personal/user/Edit.vue";
import listMyBirds from "@/views/personal/birds/Index.vue";
import showMyBird from "@/views/personal/birds/Show.vue";
import createBird from "@/views/personal/birds/Create.vue";
import createMyBird from "@/views/personal/birds/CreateMyBird.vue";
import createForeignBird from "@/views/personal/birds/CreateForeignBird.vue";
import addParents from "@/views/personal/birds/AddParents.vue";
import addMyParents from "@/views/personal/birds/AddMyParents.vue";
import addForeignParents from "@/views/personal/birds/AddForeignParents.vue";
import addBirdPhoto from "@/views/personal/birds/AddPhoto.vue";
import editBird from "@/views/personal/birds/Edit.vue";
import createRating from "@/views/personal/ratings/Create.vue";
import showMyRating from "@/views/personal/ratings/Show.vue";
import editRating from "@/views/personal/ratings/Edit.vue";
// PERSONAL HEALTH
import showHealthInfo from "@/views/personal/health/Show.vue";
// PERSONAL bird transfer
import createTransfer from "@/views/personal/transfers/Create.vue";
// PERSONAL eggs
import eggs from "@/views/personal/Eggs.vue";
import createEgg from "@/views/personal/health/eggs/Create.vue";
import editEgg from "@/views/personal/health/eggs/Edit.vue";
// PERSONAL diseases
import createDisease from "@/views/personal/health/diseases/Create.vue";
import editDisease from "@/views/personal/health/diseases/Edit.vue";
import showDisease from "@/views/personal/health/diseases/Show.vue";
// PERSONAL vaccines
import createVaccine from "@/views/personal/health/vaccines/Create.vue";
import editVaccine from "@/views/personal/health/vaccines/Edit.vue";
import showVaccine from "@/views/personal/health/vaccines/Show.vue";

// COMMON
import showRating from "@/views/common/ratings/Show.vue";
import listAllBirds from "@/views/common/birds/Index.vue";
import showTheBird from "@/views/common/birds/Show.vue";
// COMMON transfer
import birdTransferInfo from "@/views/common/transfers/Info.vue";
import birdTransferRequest from "@/views/common/transfers/Request.vue";
import birdTransferResult from "@/views/common/transfers/Result.vue";
import updateOwnerOfTheBird from "@/views/common/transfers/UpdateOwnerOfTheBird.vue";
// COMMON USER
import userList from "@/views/common/users/Index.vue";
import userShow from "@/views/common/users/Show.vue";
// COMMON pedigree
import pedigree from "@/views/common/birds/Pedigree.vue";
import searchBird from "@/views/common/birds/SearchBird.vue";

const routes = {
    login,
    register,
    selfie,
    editUser,
    listMyBirds,
    firstLoggedIn,
    accountExpired,
    unBlockRequest,
    exit,
    showMyBird,
    createBird,
    createMyBird,
    createForeignBird,
    addParents,
    addMyParents,
    addForeignParents,
    addBirdPhoto,
    editBird,
    editRating,
    createRating,
    showMyRating,
    showRating,

    eggs,
    createEgg,
    editEgg,
    showHealthInfo,
    createTransfer,

    createDisease,
    editDisease,
    showDisease,

    createVaccine,
    editVaccine,
    showVaccine,

    listAllBirds,
    showTheBird,

    birdTransferInfo,
    birdTransferRequest,
    birdTransferResult,
    updateOwnerOfTheBird,

    userList,
    userShow,
    pedigree,

    searchBird
};
export default routes;
