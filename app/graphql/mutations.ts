import { gql } from "apollo-boost";
// PERSONAL
export const CREATE_BIRD = gql`
    mutation createBird(
        $bilezik: String!
        $user: ID!
        $isim: String
        $Dogum_Tarihi: Date
        $Cinsiyet: ENUM_BIRD_CINSIYET
        $Tepe: ENUM_BIRD_TEPE
        $Turu: ENUM_BIRD_TURU
        $Sag_Kuyruk: Int
        $Sol_Kuyruk: Int
        $Atlama: Boolean
        $Notlar: String
    ) {
        createBird(
            input: {
                data: {
                    bilezik: $bilezik
                    user: $user
                    isim: $isim
                    Dogum_Tarihi: $Dogum_Tarihi
                    Cinsiyet: $Cinsiyet
                    Tepe: $Tepe
                    Turu: $Turu
                    Sag_Kuyruk: $Sag_Kuyruk
                    Sol_Kuyruk: $Sol_Kuyruk
                    Atlama: $Atlama
                    Notlar: $Notlar
                    Canli: true
                    Pasif: false
                }
            }
        ) {
            bird {
                id
                isim
                bilezik
                Dogum_Tarihi
                Turu
                Cinsiyet
                Fotograf {
                    id
                    formats
                    url
                    name
                }
                Notlar
                Tepe
                Cinsi
                Sag_Kuyruk
                Sol_Kuyruk
                Atlama
                Canli
                Pasif
                user {
                    id
                    username
                    Uye_No
                    Kulup_Uye_No
                }
                Anne {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Baba {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                diseases {
                    id
                    isim
                    Gorulme_Tarihi
                    Notlar
                }
                vaccines {
                    id
                    isim
                    Uygulama_Tarihi
                }
                races {
                    id
                    Sehir
                    Yaris_Tarihi
                    Yaris_Ad
                    Derece
                    Notlar
                }
                eggs {
                    id
                    Gorulme_Tarihi
                    Notlar
                }
                transfer {
                    id
                    Guvenlik_Kodu
                    updated_at
                }
            }
        }
    }
`;
export const ADD_PARENTS = gql`
    mutation($birdID: ID!, $anneID: ID, $babaID: ID) {
        updateBird(
            input: {
                where: { id: $birdID }
                data: { Anne: $anneID, Baba: $babaID }
            }
        ) {
            bird {
                id
                isim
                bilezik
                Dogum_Tarihi
                Turu
                Cinsiyet
                Fotograf {
                    id
                    formats
                    url
                    name
                }
                Notlar
                Tepe
                Cinsi
                Sag_Kuyruk
                Sol_Kuyruk
                Atlama
                Canli
                Pasif
                user {
                    id
                    username
                    Uye_No
                    Kulup_Uye_No
                }
                Anne {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Baba {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                diseases {
                    id
                    isim
                    Gorulme_Tarihi
                    Notlar
                }
                vaccines {
                    id
                    isim
                    Uygulama_Tarihi
                }
                races {
                    id
                    Sehir
                    Yaris_Tarihi
                    Yaris_Ad
                    Derece
                    Notlar
                }
                eggs {
                    id
                    Gorulme_Tarihi
                    Notlar
                }
                transfer {
                    id
                    Guvenlik_Kodu
                    updated_at
                }
            }
        }
    }
`;
export const ADD_PHOTO = gql`
    mutation addPhoto($birdID: ID!, $fotoID: ID) {
        updateBird(
            input: { where: { id: $birdID }, data: { Fotograf: $fotoID } }
        ) {
            bird {
                id
                isim
                bilezik
                Dogum_Tarihi
                Turu
                Cinsiyet
                Fotograf {
                    id
                    formats
                    url
                    name
                }
                Notlar
                Tepe
                Cinsi
                Sag_Kuyruk
                Sol_Kuyruk
                Atlama
                Canli
                Pasif
                user {
                    id
                    username
                    Uye_No
                    Kulup_Uye_No
                }
                Anne {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Baba {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                diseases {
                    id
                    isim
                    Gorulme_Tarihi
                    Notlar
                }
                vaccines {
                    id
                    isim
                    Uygulama_Tarihi
                }
                races {
                    id
                    Sehir
                    Yaris_Tarihi
                    Yaris_Ad
                    Derece
                    Notlar
                }
                eggs {
                    id
                    Gorulme_Tarihi
                    Notlar
                }
                transfer {
                    id
                    Guvenlik_Kodu
                    updated_at
                }
            }
        }
    }
`;
export const ADD_SELFIE = gql`
    mutation addSelfie($userID: ID!, $fotoID: ID) {
        updateUser(
            input: { where: { id: $userID }, data: { Profil_Foto: $fotoID } }
        ) {
            user {
                id
                username
                Uye_No
                Kulup_Uye_No
                created_at
                updated_at
                email
                blocked
                Uyelik_Yenileme
                birds(where: { Canli: true }, limit: 16) {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Profil_Foto {
                    previewUrl
                    url
                }
            }
        }
    }
`;

export const EDIT_BIRD = gql`
    mutation(
        $birdID: ID!
        $isim: String
        $Dogum_Tarihi: Date
        $Cinsiyet: ENUM_BIRD_CINSIYET
        $Tepe: ENUM_BIRD_TEPE
        $Turu: ENUM_BIRD_TURU
        $Sag_Kuyruk: Int
        $Sol_Kuyruk: Int
        $Atlama: Boolean
        $Notlar: String
        $Canli: Boolean
        $userID: ID
    ) {
        updateBird(
            input: {
                where: { id: $birdID }
                data: {
                    isim: $isim
                    Dogum_Tarihi: $Dogum_Tarihi
                    Cinsiyet: $Cinsiyet
                    Tepe: $Tepe
                    Turu: $Turu
                    Sag_Kuyruk: $Sag_Kuyruk
                    Sol_Kuyruk: $Sol_Kuyruk
                    Atlama: $Atlama
                    Notlar: $Notlar
                    Canli: $Canli
                    user: $userID
                }
            }
        ) {
            bird {
                id
                isim
                bilezik
                Dogum_Tarihi
                Turu
                Cinsiyet
                Fotograf {
                    id
                    formats
                    url
                    name
                }
                Notlar
                Tepe
                Cinsi
                Sag_Kuyruk
                Sol_Kuyruk
                Atlama
                Canli
                Pasif
                user {
                    id
                    username
                    Uye_No
                    Kulup_Uye_No
                }
                Anne {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Baba {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                diseases {
                    id
                    isim
                    Gorulme_Tarihi
                    Notlar
                }
                vaccines {
                    id
                    isim
                    Uygulama_Tarihi
                }
                races {
                    id
                    Sehir
                    Yaris_Tarihi
                    Yaris_Ad
                    Derece
                    Notlar
                }
                eggs {
                    id
                    Gorulme_Tarihi
                    Notlar
                }
                transfer {
                    id
                    Guvenlik_Kodu
                    updated_at
                }
            }
        }
    }
`;
export const EDIT_USER = gql`
    mutation updateUser(
        $userID: ID!
        $username: String
        $email: String
        $password: String
        $uyeNo: Int
        $kulupUyeNo: Int
        $ulkeKodu: String
        $plakaKodu: String
        $telefon: String
        $sehir: String
) {
  updateUser(
    input: { 
      where: { id: $userID },
      data: {
        username: $username, 
        email: $email, 
        password: $password,
        Uye_No: $uyeNo,
        Kulup_Uye_No: $kulupUyeNo,
        Ulke_Kodu: $ulkeKodu, 
        Plaka_Kodu: $plakaKodu,
        Telefon: $telefon, 
        Sehir: $sehir
      }
    }
  ) {
    user {
      username
      email
      Uye_No
      Kulup_Uye_No
      Plaka_Kodu
      Ulke_Kodu
      Sehir
      Telefon
    }
  }
}

`;

export const CREATE_RATING = gql`
    mutation createRating(
        $Yaris_Ad: String!
        $Sehir: String!
        $Notlar: String
        $bird: ID!
        $Derece: Int
        $Yaris_Tarihi: Date
    ) {
        createRace(
            input: {
                data: {
                    Yaris_Ad: $Yaris_Ad
                    Sehir: $Sehir
                    Notlar: $Notlar
                    bird: $bird
                    Derece: $Derece
                    Yaris_Tarihi: $Yaris_Tarihi
                }
            }
        ) {
            race {
                id
                Yaris_Ad
                Notlar
                Sehir
                Derece
                Yaris_Tarihi
            }
        }
    }
`;
export const EDIT_RATING = gql`
    mutation editRating(
        $raceID: ID!
        $Yaris_Ad: String
        $Sehir: String
        $Notlar: String
        $Derece: Int
        $Yaris_Tarihi: Date
    ) {
        updateRace(
            input: {
                where: { id: $raceID }
                data: {
                    Yaris_Ad: $Yaris_Ad
                    Sehir: $Sehir
                    Notlar: $Notlar
                    Derece: $Derece
                    Yaris_Tarihi: $Yaris_Tarihi
                }
            }
        ) {
            race {
                id
                Yaris_Ad
                Notlar
                Sehir
                Derece
                Yaris_Tarihi
            }
        }
    }
`;

export const CREATE_DISEASE = gql`
    mutation createDisease(
        $isim: String!
        $Gorulme_Tarihi: Date
        $Notlar: String
        $birdID: ID!
    ) {
        createDisease(
            input: {
                data: {
                    isim: $isim
                    Gorulme_Tarihi: $Gorulme_Tarihi
                    Notlar: $Notlar
                    bird: $birdID
                }
            }
        ) {
            disease {
                id
                isim
                Gorulme_Tarihi
                Notlar
            }
        }
    }
`;
export const DESTROY_DISEASE = gql`
    mutation destroyDisease($diseaseID: ID!) {
        deleteDisease(input: { where: { id: $diseaseID } }) {
            disease {
                id
                isim
                Gorulme_Tarihi
                Notlar
            }
        }
    }
`;
export const EDIT_DISEASE = gql`
    mutation editdisease(
        $isim: String!
        $Gorulme_Tarihi: Date
        $Notlar: String
        $diseaseID: ID!
    ) {
        updateDisease(
            input: {
                where: { id: $diseaseID }
                data: {
                    isim: $isim
                    Gorulme_Tarihi: $Gorulme_Tarihi
                    Notlar: $Notlar
                }
            }
        ) {
            disease {
                id
                isim
                Gorulme_Tarihi
                Notlar
                bird {
                    id
                }
            }
        }
    }
`;

export const CREATE_VACCINE = gql`
    mutation createVaccine(
        $isim: String!
        $Uygulama_Tarihi: Date
        $Notlar: String
        $birdID: ID!
    ) {
        createVaccine(
            input: {
                data: {
                    isim: $isim
                    Uygulama_Tarihi: $Uygulama_Tarihi
                    Notlar: $Notlar
                    bird: $birdID
                }
            }
        ) {
            vaccine {
                id
                isim
                Uygulama_Tarihi
                Notlar
            }
        }
    }
`;
export const EDIT_VACCINE = gql`
    mutation editVaccine(
        $isim: String!
        $Uygulama_Tarihi: Date
        $Notlar: String
        $vaccineID: ID!
    ) {
        updateVaccine(
            input: {
                where: { id: $vaccineID }
                data: {
                    isim: $isim
                    Uygulama_Tarihi: $Uygulama_Tarihi
                    Notlar: $Notlar
                }
            }
        ) {
            vaccine {
                id
                isim
                Uygulama_Tarihi
                Notlar
            }
        }
    }
`;
export const DESTROY_VACCINE = gql`
    mutation destroyVaccine($vaccineID: ID!) {
        deleteVaccine(input: { where: { id: $vaccineID } }) {
            vaccine {
                id
                isim
                Uygulama_Tarihi
                Notlar
            }
        }
    }
`;

export const CREATE_EGG = gql`
    mutation createEgg($Gorulme_Tarihi: Date!, $Notlar: String, $birdID: ID!) {
        createEgg(
            input: {
                data: {
                    Gorulme_Tarihi: $Gorulme_Tarihi
                    Notlar: $Notlar
                    bird: $birdID
                }
            }
        ) {
            egg {
                id
                Gorulme_Tarihi
                Notlar
            }
        }
    }
`;
export const EDIT_EGG = gql`
    mutation editEgg($Gorulme_Tarihi: Date, $Notlar: String, $eggID: ID!) {
        updateEgg(
            input: {
                where: { id: $eggID }
                data: { Gorulme_Tarihi: $Gorulme_Tarihi, Notlar: $Notlar }
            }
        ) {
            egg {
                id
                Gorulme_Tarihi
                Notlar
                bird {
                    id
                }
            }
        }
    }
`;
export const DESTROY_EGG = gql`
    mutation destroyEgg($eggID: ID!) {
        deleteEgg(input: { where: { id: $eggID } }) {
            egg {
                id
                Gorulme_Tarihi
                Notlar
                bird {
                    id
                }
            }
        }
    }
`;

export const CREATE_TRANSFER_CODE = gql`
    mutation createTransferCode($birdID: ID!, $Guvenlik_Kodu: String!) {
        createTransfer(
            input: { data: { Guvenlik_Kodu: $Guvenlik_Kodu, bird: $birdID } }
        ) {
            transfer {
                id
                Guvenlik_Kodu
                updated_at
            }
        }
    }
`;
export const DISABLE_BIRD = gql`
    mutation disableBird($birdID: ID!) {
        updateBird(input: { where: { id: $birdID }, data: { Pasif: true } }) {
            bird {
                id
                isim
                bilezik
                Dogum_Tarihi
                Turu
                Cinsiyet
                Fotograf {
                    id
                    formats
                    url
                    name
                }
                Notlar
                Tepe
                Cinsi
                Sag_Kuyruk
                Sol_Kuyruk
                Atlama
                Canli
                Pasif
                user {
                    id
                    username
                    Uye_No
                    Kulup_Uye_No
                }
                Anne {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Baba {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                diseases {
                    id
                    isim
                    Gorulme_Tarihi
                    Notlar
                }
                vaccines {
                    id
                    isim
                    Uygulama_Tarihi
                }
                races {
                    id
                    Sehir
                    Yaris_Tarihi
                    Yaris_Ad
                    Derece
                    Notlar
                }
                eggs {
                    id
                    Gorulme_Tarihi
                    Notlar
                }
                transfer {
                    id
                    Guvenlik_Kodu
                    updated_at
                }
            }
        }
    }
`;

export const HIDE_MY_INFO = gql`
    mutation hideMyInfo($userID: ID!) {
        updateUser(input: { where: { id: $userID }, data: { Gizli: true } }) {
            user {
                id
                username
                Uye_No
                Kulup_Uye_No
                created_at
                updated_at
                email
                blocked
                Uyelik_Yenileme
                birds(where: { Canli: true, Pasif: false }) {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Profil_Foto {
                    previewUrl
                    url
                }
                Plaka_Kodu
                Ulke_Kodu
                Sehir
                Telefon
                Gizli
            }
        }
    }
`;
export const SHOW_MY_INFO = gql`
    mutation hideMyInfo($userID: ID!) {
        updateUser(input: { where: { id: $userID }, data: { Gizli: false } }) {
            user {
                id
                username
                Uye_No
                Kulup_Uye_No
                created_at
                updated_at
                email
                blocked
                Uyelik_Yenileme
                birds(where: { Canli: true, Pasif: false }) {
                    id
                    bilezik
                    isim
                    Cinsiyet
                }
                Profil_Foto {
                    previewUrl
                    url
                }
                Plaka_Kodu
                Ulke_Kodu
                Sehir
                Telefon
                Gizli
            }
        }
    }
`;
