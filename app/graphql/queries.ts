import { gql } from "apollo-boost";
// PERSONAL
export const MY_ALIVE_BIRDS = gql`
    query myAliveBirds($userId: ID!, $sort: String) {
        birds(
            where: { Canli: true, Pasif: false, user: $userId }
            sort: $sort
        ) {
            id
            isim
            bilezik
            Dogum_Tarihi
            Turu
            Cinsiyet
            Fotograf {
                id
                width
                height
                formats
                url
                name
            }
            Notlar
            Tepe
            Cinsi
            Sag_Kuyruk
            Sol_Kuyruk
            Atlama
            Canli
            Pasif
            user {
                id
                username
                Uye_No
                Kulup_Uye_No
            }
            Anne {
                id
                bilezik
                isim
                Cinsiyet
            }
            Baba {
                id
                bilezik
                isim
                Cinsiyet
            }
            diseases {
                id
                isim
                Gorulme_Tarihi
                Notlar
            }
            vaccines {
                id
                isim
                Uygulama_Tarihi
            }
            races {
                id
                Sehir
                Yaris_Tarihi
                Yaris_Ad
                Derece
                Notlar
            }
            eggs {
                id
                Gorulme_Tarihi
                Notlar
            }
            transfer {
                id
                Guvenlik_Kodu
                updated_at
            }
        }
    }
`;
export const BIRDS_WITH_EGGS = gql`
    query birdsWithEggs($limit: Int = 32, $userId: Int!) {
        birds(
            where: { Canli: true, user: $userId, Cinsiyet: "disi", eggs_gt: 0 }
            limit: $limit
        ) {
            id
            isim
            bilezik
            eggs {
                id
                Gorulme_Tarihi
                Notlar
            }
        }
    }
`;
export const BIRD = gql`
    query bird($id: ID!) {
        bird(id: $id) {
            id
            isim
            bilezik
            Dogum_Tarihi
            Turu
            Cinsiyet
            Fotograf {
                id
                width
                height
                formats
                url
                name
            }
            Notlar
            Tepe
            Cinsi
            Sag_Kuyruk
            Sol_Kuyruk
            Atlama
            Canli
            Pasif
            user {
                id
                username
                Uye_No
                Kulup_Uye_No
            }
            Anne {
                id
                bilezik
                isim
                Cinsiyet
            }
            Baba {
                id
                bilezik
                isim
                Cinsiyet
            }
            diseases {
                id
                isim
                Gorulme_Tarihi
                Notlar
            }
            vaccines {
                id
                isim
                Uygulama_Tarihi
            }
            races {
                id
                Sehir
                Yaris_Tarihi
                Yaris_Ad
                Derece
                Notlar
            }
            eggs {
                id
                Gorulme_Tarihi
                Notlar
            }
            transfer {
                id
                Guvenlik_Kodu
                updated_at
            }
        }
    }
`;
export const MY_ALIVE_MALE_BIRDS = gql`
    query myAliveMaleBirds($userId: ID!) {
        birds(
            where: { Canli: true, user: $userId, Cinsiyet: "erkek" }
            sort: "bilezik"
        ) {
            id
            isim
            bilezik
        }
    }
`;
export const MY_ALIVE_FEMALE_BIRDS = gql`
    query myAliveFemaleBirds($userId: ID!) {
        birds(
            where: { Canli: true, user: $userId, Cinsiyet: "disi" }
            sort: "bilezik"
        ) {
            id
            isim
            bilezik
        }
    }
`;
// DISEASES
export const DISEASE = gql`
    query disease($diseaseID: ID!) {
        disease(id: $diseaseID) {
            id
            isim
            Gorulme_Tarihi
            Notlar
            bird {
                id
            }
        }
    }
`;
export const DISEASES_OF_BIRD = gql`
    query diseasesOfBird($limit: Int = 32, $birdID: ID!) {
        diseases(where: { bird: $birdID }, limit: $limit) {
            id
            isim
            Gorulme_Tarihi
            Notlar
        }
    }
`;
// VACCINES
export const VACCINES_OF_BIRD = gql`
    query vaccinesOfBird($limit: Int = 32, $birdID: ID!) {
        vaccines(where: { bird: $birdID }, limit: $limit) {
            id
            isim
            Uygulama_Tarihi
            Notlar
        }
    }
`;
export const THE_VACCINE = gql`
    query theVaccine($vaccineID: ID!) {
        vaccine(id: $vaccineID) {
            id
            isim
            Uygulama_Tarihi
            Notlar
        }
    }
`;
// EGGS
export const EGGS_OF_BIRD = gql`
    query eggsOfBird($limit: Int = 32, $birdID: ID!) {
        eggs(where: { bird: $birdID }, limit: $limit) {
            id
            Gorulme_Tarihi
            Notlar
        }
    }
`;
// COMMON
export const BIRD_FROM_BILEZIK = gql`
    query bird($bilezik: String!) {
        birds(where: { bilezik: $bilezik }, limit: 1) {
            id
            isim
        }
    }
`;
export const MALE_BIRD_FROM_BILEZIK = gql`
    query bird($bilezik: String!) {
        birds(
            where: { bilezik: $bilezik, Pasif: false, Cinsiyet: "erkek" }
            limit: 1
        ) {
            id
            isim
        }
    }
`;
export const FEMALE_BIRD_FROM_BILEZIK = gql`
    query bird($bilezik: String!) {
        birds(
            where: { bilezik: $bilezik, Pasif: false, Cinsiyet: "disi" }
            limit: 1
        ) {
            id
            isim
        }
    }
`;
export const USERS_LIST = gql`
    query users($start: Int) {
        users(where: { blocked: false }, sort: "username", start: $start) {
            id
            username
            Uye_No
            Kulup_Uye_No
        }
    }
`;
export const USER = gql`
    query user($userID: ID!) {
        user(id: $userID) {
            id
            username
            Uye_No
            Kulup_Uye_No
            created_at
            updated_at
            email
            blocked
            Uyelik_Yenileme
            birds(where: { Canli: true, Pasif: false }) {
                id
                bilezik
                isim
                Cinsiyet
            }
            Profil_Foto {
                previewUrl
                url
            }
            Plaka_Kodu
            Ulke_Kodu
            Sehir
            Telefon
            Gizli
        }
    }
`;
export const ALL_ALIVE_BIRDS = gql`
    query allAliveBirds($limit: Int = 300) {
        birds(
            where: { Canli: true, Pasif: false }
            limit: $limit
            sort: "id:DESC"
        ) {
            id
            isim
            bilezik
            Cinsiyet
        }
    }
`;
export const PEDIGREE = gql`
    query bird($id: ID!) {
        bird(id: $id) {
            id
            isim
            bilezik
            Dogum_Tarihi
            Turu
            Cinsiyet
            Canli
            Fotograf {
                id
                formats
                url
                name
            }
            user {
                id
                username
                Uye_No
                Kulup_Uye_No
                Ulke_Kodu
                Plaka_Kodu
            }
            races {
                id
                Yaris_Ad
                Notlar
                Sehir
                Derece
                Yaris_Tarihi
            }
            Anne {
                id
                bilezik
                isim
                Dogum_Tarihi
                Turu
                Canli
                races {
                    id
                    Yaris_Ad
                    Notlar
                    Sehir
                    Derece
                    Yaris_Tarihi
                }
                Anne {
                    id
                    bilezik
                    isim
                    Dogum_Tarihi
                    Turu
                    Canli
                    races {
                        id
                        Yaris_Ad
                        Notlar
                        Sehir
                        Derece
                        Yaris_Tarihi
                    }
                    Anne {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                    Baba {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                }
                Baba {
                    id
                    bilezik
                    isim
                    Dogum_Tarihi
                    Turu
                    Canli
                    races {
                        id
                        Yaris_Ad
                        Notlar
                        Sehir
                        Derece
                        Yaris_Tarihi
                    }
                    Anne {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                    Baba {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                }
            }
            Baba {
                id
                bilezik
                isim
                Dogum_Tarihi
                Turu
                Canli
                races {
                    id
                    Yaris_Ad
                    Notlar
                    Sehir
                    Derece
                    Yaris_Tarihi
                }
                Anne {
                    id
                    bilezik
                    isim
                    Dogum_Tarihi
                    Turu
                    Canli
                    races {
                        id
                        Yaris_Ad
                        Notlar
                        Sehir
                        Derece
                        Yaris_Tarihi
                    }
                    Anne {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                    Baba {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                }
                Baba {
                    id
                    bilezik
                    isim
                    Dogum_Tarihi
                    Turu
                    Canli
                    races {
                        id
                        Yaris_Ad
                        Notlar
                        Sehir
                        Derece
                        Yaris_Tarihi
                    }
                    Anne {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                    Baba {
                        id
                        bilezik
                        isim
                        Dogum_Tarihi
                        Turu
                        Canli
                        races {
                            id
                            Yaris_Ad
                            Notlar
                            Sehir
                            Derece
                            Yaris_Tarihi
                        }
                    }
                }
            }
        }
    }
`;
export const THE_RACE = gql`
    query theRace($raceID: ID!) {
        race(id: $raceID) {
            id
            Yaris_Ad
            Notlar
            Sehir
            Derece
            Yaris_Tarihi
        }
    }
`;
export const TRANSFER_OF_THE_BIRD = gql`
    query transferOfBird($birdID: ID!, $today: DateTime) {
        transfers(where: { updated_ad: $today, bird: { id: $birdID } }) {
            id
            bird {
                id
                isim
            }
            Guvenlik_Kodu
            updated_at
        }
    }
`;
export const TRANSFER_OF_THE_BIRD_FROM_BILEZIK = gql`
    query transferOfBirdFromBilezik($birdBilezik: String!) {
        transfers(where: { bird: { bilezik: $birdBilezik } }) {
            id
            bird {
                id
                isim
            }
            Guvenlik_Kodu
            updated_at
        }
    }
`;
// QUERY
export const CACHE_QUERY = gql`
    query cacheQuery {
        data {
            id
            isim
            bilezik
        }
    }
`;
