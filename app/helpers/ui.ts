import { Toasty, ToastDuration } from "@triniwiz/nativescript-toasty";
import { format, formatDistanceToNow } from "date-fns";
import { tr } from "date-fns/locale";
import routes from "../routes";
import { capitalize } from "lodash";
import { Dialogs } from "@nativescript/core";

function showToast(metin: string, long?: boolean) {
    const toast: Toasty = new Toasty({ text: metin });
    toast.backgroundColor = "#e51873";
    toast.duration = long ? ToastDuration.LONG : ToastDuration.SHORT;
    toast.show();
    console.log(metin);
}
function alert(title: string, message?: string) {
    Dialogs.alert({
        title,
        message,
        okButtonText: "Tamam",
    });
}

const vocaMixin = {
    methods: {
        addLik(num: number) {
            const sonBasamak = num % 10;
            switch (sonBasamak) {
                case 6:
                    return num + ". lık";
                    break;
                case 3:
                case 4:
                    return num + ". lük";
                case 9:
                case 0:
                    return num + ". luk";
                default:
                    return num + ". lik";
            }
        },
        titleCase(text: string) {
            return text.split(" ").map(capitalize).join(" ");
        },
        turu(Turu: string) {
            return Turu == "kurenk"
                ? "Kürenk"
                : Turu == "miski"
                    ? "Miski"
                    : Turu == "arap"
                        ? "Arap"
                        : Turu == "mavi"
                            ? "Mavi"
                            : Turu == "cakmakli"
                                ? "Çakmaklı"
                                : Turu == "cil"
                                    ? "Çil"
                                    : Turu == "abali_miski"
                                        ? "Abalı Miski"
                                        : Turu == "abali_cakmakli"
                                            ? "Abalı Çakmaklı"
                                            : "Diğer";
        },
        sexWord(sex: string) {
            return sex == "disi" ? "Dişi" : sex == "erkek" ? "Erkek" : "Bebek";
        },
        sexIcon(cinsiyet: string) {
            return cinsiyet == "disi" ? "" : cinsiyet == "erkek" ? "" : "";
        },
        cinsi(cins: string) {
            return cins == "Sebap" ? "Şebap" : "Diğer";
        },
        tepesi(Tepe: string) {
            return Tepe == "duz" ? "Düz" : "Kepez";
        },
        kuyrugu(Sag_Kuyruk: string, Sol_Kuyruk: string) {
            if (Sag_Kuyruk || Sol_Kuyruk) {
                const kuyruk = Sag_Kuyruk + " 'e " + Sol_Kuyruk;
                return this.Atlama
                    ? kuyruk + " Atlamalı"
                    : kuyruk + " Atlamasız";
            }
        },
    },
};
const navMixin = {
    methods: {

        gotoListMyBirds() {
            this.$navigateTo(routes.listMyBirds);
        },
        gotoCreateBird() {
            this.$navigateTo(routes.createBird);
        },
        gotoCreateForeignBird() {
            this.$navigateTo(routes.createForeignBird);
        },
        gotoCreateMyBird() {
            this.$navigateTo(routes.createMyBird);
        },
        gotoListAllBirds() {
            this.$navigateTo(routes.listAllBirds);
        },
        gotoBirdTransferInfo() {
            this.$navigateTo(routes.birdTransferInfo);
        },
        gotoUserList() {
            this.$navigateTo(routes.userList);
        },
        gotoRegister() {
            this.$navigateTo(routes.register);
        },
        gotoUserShow(userID: number | string) {
            this.$navigateTo(routes.userShow, {
                props: {
                    userID,
                },
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 380,
                    curve: "easeIn",
                },
            });
        },
        gotoEggs() {
            this.$navigateTo(routes.eggs);
        },
        gotoShowMyBird(birdID: number | string) {
            this.$navigateTo(routes.showMyBird, {
                props: { birdID: birdID },
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 380,
                    curve: "easeIn",
                },
            });
        },
        gotoShowTheBird(birdID: number | string) {
            this.$navigateTo(routes.showTheBird, {
                props: { birdID: birdID },
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 380,
                    curve: "easeIn",
                },
            });
        },
        gotoSearchBird() {
            this.$navigateTo(routes.searchBird);
        },
        gotoPedigree(birdID: number | string) {
            this.$navigateTo(routes.pedigree, {
                props: { birdID: birdID },
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 380,
                    curve: "easeIn",
                },
            });
        },
        gotoTransferRequest() {
            this.$navigateTo(routes.birdTransferRequest);
        },
        gotoSelfie() {
            this.$navigateTo(routes.selfie);
        },
        gotoEditUser() {
            this.$navigateTo(routes.editUser);
        },
        gotoHealth(birdID: number | string) {
            this.$navigateTo(routes.showHealthInfo, {
                props: {
                    birdID: birdID,
                },
            });
        },
        gotoShowMyRace(
            raceID: number | string,
            birdName: string,
            birdBilezik: string
        ) {
            this.$navigateTo(routes.showMyRating, {
                props: {
                    raceID,
                    birdName,
                    birdBilezik,
                },
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 380,
                    curve: "easeIn",
                },
            });
        },
        gotoEditEgg(
            birdID: number | string,
            eggID: number | string,
            Gorulme_Tarihi: string,
            Notlar: string
        ) {
            // console.log("clicked ", eggID);
            this.$navigateTo(routes.editEgg, {
                props: {
                    birdID,
                    eggID,
                    Gorulme_Tarihi,
                    Notlar,
                },
            });
        },
        gotoDisease(diseaseID: number | string, birdID: number | string) {
            this.$navigateTo(routes.showDisease, {
                props: { diseaseID, birdID },
            });
        },

        gotoCreateVaccine(birdID: number | string) {
            this.$navigateTo(routes.createVaccine, {
                props: { birdID },
            });
        },
        gotoCreateDisease(birdID: number | string) {
            this.$navigateTo(routes.createDisease, {
                props: { birdID },
            });
        },
        gotoCreateEgg(birdID: number | string) {
            this.$navigateTo(routes.createEgg, {
                props: { birdID },
            });
        },
        gotoShowVaccine(vaccineID: number | string, birdID: number | string) {
            this.$navigateTo(routes.showVaccine, {
                props: { vaccineID: vaccineID, birdID: birdID },
            });
        },
        gotoExit() {
            this.$navigateTo(routes.exit);
        },
    },
};

const dateMixin = {
    methods: {
        turkishDate(date: string) {
            return format(Date.parse(date), "d MMM yyyy", {
                locale: tr,
            });
        },
        getMemberSince(created_at: string) {
            this.memberSince = formatDistanceToNow(Date.parse(created_at), {
                locale: tr,
            });
        },
        birthDate(Dogum_Tarihi: string) {
            return format(Date.parse(Dogum_Tarihi), "d MMM yy", {
                locale: tr,
            });
        },
        beforeTime(date: string) {
            return (
                formatDistanceToNow(Date.parse(date), { locale: tr }) + " önce"
            );
        },
    },
};

///////////////////////// Colored Console

[
    ["warn", "\x1b[1;33m"],
    ["error", "\x1b[1;31m"],
].forEach((pair) => {
    const method = pair[0];
    const reset = "\x1b[0m";
    const color = pair[1];
    console[method] = console[method].bind(
        console,
        color,
        method.toUpperCase(),
        reset
    );
});
interface Console {
    errorColor(message?: any, ...optionalParams: Array<any>): void;
    warnColor(message?: any, ...optionalParams: Array<any>): void;
}

export { showToast, vocaMixin, dateMixin, navMixin, alert };
