import { add, parseISO, format } from "date-fns";
import { LocalNotifications } from "@nativescript/local-notifications";
import { showToast, alert } from "@/helpers/ui";
import { Color, ApplicationSettings, isAndroid, SearchBar } from "@nativescript/core";
import { tr } from "date-fns/locale";
import * as application from "@nativescript/core/application";
import { apolloClient } from "@/main";
import routes from "@/routes";

const hachingPeriod = { days: 21, hours: 15 };

function scheduleNotification(gorulme_Tarihi: string, username: string) {
    const hatchingDate = add(parseISO(gorulme_Tarihi), hachingPeriod);
    const scheduleOption: {} = {
        //id: 2,
        title: username,
        subtitle: "Yumurta bugün çıkıyor!",
        body:
            "21 gün önce görülen yumurtanızın detaylarını görmek için SebApp uygulamasına bakınız.",
        bigTextStyle: true,
        color: new Color("magenta"),
        thumbnail: "https://sebapi.com/sebAPP-logo-k.png",
        forceShowWhenInForeground: true,
        channel: "SebApp",
        ticker: "SepApp Yumurta",
        at: new Date(hatchingDate),
        //at: new Date(new Date().getTime() + 5000), // 5 seconds from now
        actions: [
            {
                id: "yes",
                type: "button",
                title: "Göster",
                launch: true,
            },
            {
                id: "no",
                type: "button",
                title: "Sil",
                launch: false,
            },
        ],
    };
    LocalNotifications.schedule([scheduleOption])
        .then(() => {
            //console.log('scheduleOption :>> ', scheduleOption);
            const tarih = (format(hatchingDate, "dd MMMM yyyy 'saat ' HH:mm ", { locale: tr }));
            showToast(tarih + "için Hatırlatıcı eklendi", true);
        })
        .catch((error) => console.log("doSchedule error: " + error));
};
function removeData() {
    ApplicationSettings.remove("token");
    ApplicationSettings.remove("userID");
    ApplicationSettings.remove("userData");
    apolloClient.clearStore();
};
const domainMixin = {
    methods: {
        bilezikFormInfoIsValid(bilezikForm): boolean {
            if (
                !(bilezikForm.kusSiraNo > 0) ||
                (!(bilezikForm.uyeNo > 0) && !(bilezikForm.kulupUyeNo > 0)) ||
                !(bilezikForm.plakaKodu > 0)
            ) {
                return false;
            } else {
                return true;
            }
        },
        closeApp() {
            if (application.android) {
                application.android.foregroundActivity.finish();
            } else {
                exit(0);
            }
        },
        exit() {
            removeData();
            this.cancelReminders();
            this.$navigateTo(routes.login);
        },
        about() {
            alert("Geliştirici", "cem@cemkaan.com");
        },
        cancelReminders() {
            LocalNotifications.cancelAll()
                .then(() => {
                    showToast("Tüm veriler ve hatırlatıcılar Temizlendi", true);
                    this.$navigateTo(routes.exit);
                })
                .catch((error) => console.log("doCancelAll error: " + error));
        },
        async getCountOfReminders(): Promise<number> {
            let count: number;
            await LocalNotifications.getScheduledIds()
                .then(
                    (arrayOfIds: []) => {
                        count = arrayOfIds.length
                    }
                );
            return count;
        },
        sBLoaded(args) {
            var searchbar: SearchBar = <SearchBar>args.object;
            if (isAndroid) {
                searchbar.android.clearFocus();
            }
        },
    },
}

export { scheduleNotification, domainMixin, removeData };
